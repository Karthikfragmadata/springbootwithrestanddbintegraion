package com.fragmadata.DbConnectivity.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@ConfigurationProperties(prefix = "ds.props")
public class DataSourceConfig {

    private String username;
    private String password;
    private String url;
    private String driver;
    private int connectionPoolSize;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public int getConnectionPoolSize() {
        return connectionPoolSize;
    }

    public void setConnectionPoolSize(int connectionPoolSize) {
        this.connectionPoolSize = connectionPoolSize;
    }


    @Bean
    public DataSource dataSource() {

        HikariDataSource ds = new HikariDataSource();
        //logger.info("HikariDataSource=" + ds);
        ds.setMaximumPoolSize(connectionPoolSize);
        ds.setDriverClassName(this.getDriver());
        ds.setJdbcUrl(this.getUrl());
        ds.setUsername(this.getUsername());
        ds.setPassword(this.getPassword());
        ds.setAutoCommit(false);
        ds.setInitializationFailFast(false);
        return ds;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() throws SQLException {
        return new JdbcTemplate(dataSource());
    }


    @Override
    public String toString() {
        return "DataSourceConfig{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", url='" + url + '\'' +
                ", driver='" + driver + '\'' +
                ", connectionPoolSize=" + connectionPoolSize +
                '}';
    }

}
