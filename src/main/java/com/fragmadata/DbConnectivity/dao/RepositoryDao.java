package com.fragmadata.DbConnectivity.dao;


import com.fragmadata.DbConnectivity.model.Employee;
import com.fragmadata.DbConnectivity.model.Employees;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RepositoryDao {

    private JdbcTemplate jdbcTemplate;

    private static Logger logger = LoggerFactory.getLogger(RepositoryDao.class);

    @Autowired
    public RepositoryDao(JdbcTemplate jdbcTemplate) {

        this.jdbcTemplate = jdbcTemplate;

    }

    private static String ALL_EMPLOYEES_QUERY = "SELECT * FROM employee";

    private static String EMPLOYEE_BASED_ON_ID = "SELECT * FROM employee WHERE ID = ?";

    private static String EMPLOYEES_QUERY_BY_FILTER = "SELECT * FROM employee WHERE id!=?";

    private static String INSERT_EMPLOYEE_QUERY = "INSERT INTO employee(FIRST_NAME,LAST_NAME,EMAIL) VALUES(?,?,?)";

    /**
     * WITH OUT PREPARED STATEMENT AND COMPULSARY RETURNS LIST OF OBJECTS
     */
    public Employees getAllEmployees() {

        logger.info("INSIDE THE GET ALL EMPLOYEES METHOD..");

        logger.info("ALL_EMPLOYEES_DATA QUERY=" + ALL_EMPLOYEES_QUERY);

        Employees employees = new Employees();

        List<Employee> data = jdbcTemplate.query(ALL_EMPLOYEES_QUERY, new RowMapper<Employee>() {

                    public Employee mapRow(ResultSet rs, int rowNum)
                            throws SQLException {

                        int id = rs.getInt("id");
                        String first_name = rs.getString("first_name");
                        String last_name = rs.getString("last_name");
                        String email = rs.getString("email");

                        Employee employee = new Employee(id, first_name, last_name, email);

                        return employee;
                    }

                }

        );
        logger.info("data=" + data);
        employees.setEmployeeList(data);

        return employees;
    }


    /**
     * WITH PREPARED STATEMENT WITH COMPULSARY RETURN TYPE as List
     * RowMapper
     */
    public Employee getAnEmployeeDataByIdRowMapper(int id) {

        List<Employee> listOfEmployees = jdbcTemplate.query(


                new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

                        PreparedStatement ps = con.prepareStatement(EMPLOYEE_BASED_ON_ID);
                        ps.setInt(1, id);
                        return ps;
                    }
                },
                new RowMapper<Employee>() {
                    @Override
                    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {

                        int id = rs.getInt("id");
                        String first_name = rs.getString("first_name");
                        String last_name = rs.getString("last_name");
                        String email = rs.getString("email");

                        Employee employee = new Employee(id, first_name, last_name, email);

                        return employee;
                    }
                });

        return listOfEmployees.get(0);
    }


    /**
     * WITH PREPARED STATEMENT NO COMPULSARY RETURN TYPE
     * ROWCALLBACKHANDLER
     */
    public List<Employee> getEmployeesbyExcludingOneIdnRowCallBackHandler(int id) {

        //logger.info();
        List<Employee> listOfEmployees = new ArrayList<>();

        jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

                PreparedStatement ps = connection.prepareStatement(EMPLOYEES_QUERY_BY_FILTER);
                ps.setInt(1, id);
                return ps;
            }
        }, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {

                int id = rs.getInt("id");
                String first_name = rs.getString("first_name");
                String last_name = rs.getString("last_name");
                String email = rs.getString("email");

                listOfEmployees.add(new Employee(id, first_name, last_name, email));

            }
        });

        return listOfEmployees;
    }


    @Transactional
    public int addEmployee(Employee employee) {

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(INSERT_EMPLOYEE_QUERY, new String[]{"ID"});

                ps.setString(1, employee.getFirstName());
                ps.setString(2, employee.getLastName());
                ps.setString(3, employee.getEmail());

                return ps;
            }
        }, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Transactional
    public void executeTransactionTest(Employee employee) {

        int i = addEmployee(employee);
        logger.info("AGID=" + i);

        throw new RuntimeException();
    }


}
